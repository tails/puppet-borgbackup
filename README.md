# borgbackup

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with borgbackup](#setup)
    * [Backup server](#backup-server)
    * [Backup client](#backup-client)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Development - Guide for contributing to the module](#development)

## Description

This module is intended to facilitate a backup scheme based on 
[borg](https://www.borgbackup.org) with the following security properties:

  - compromise of a node cannot escalate to compromise of backups
  - compromise of the backup server cannot escalate to compromise of nodes

To accomplish this, the backup server is assumed to be managed masterless and
nodes are restricted to append-only access.

This has a number of downsides:

  - access to the backupserver has to be configured manually and cannot be
    automated with exported resources.
  - purging of old backups will have to be done manually.

If you are looking for a backup solution with as little configuration effort as
possible, you are probably better off with the
[Vox Populi module](https://forge.puppet.com/modules/puppet/borg/readme).

## Setup

**Important:** to be able to recover from backups, you will need access to the
following three:

  - the BORG_PASSPHRASE, set is borgbackup::client::borgpassphrase
  - ssh access to the backup server
  - the client's keyfile, stored in /root/.config/borg/keys

This module does **not** arrange these requirements for you! Make sure you have
local copies of the BORG_PASSPHRASE as well as the clients' keys on the machine
you want to use for recovery.

### Backup server

The backup server can be configured as follows:

```
include borgbackup::server
```

To grant nodes access to write backups, fill your hiera with something like:

```
borgbackups::server::clients:
  hostname1:
    type: ssh-rsa
    data: AAAAgnove41...
  hostname2:
    type: ssh-rsa
    data: AAAAnbor0ve..
```

This will create users named hostname1, hostname2, etc. You will need to make
sure you provide yourself (or anyone who needs access to the backups) with ssh
access to these accounts.

### Backup client

All requirements for running backup jobs can be installed as follows:

```
include borgbackup::client
```

This class is included in the backup_lv and backup_fs defined types, so there
is no strict need to do so explicitly in your own manifests.

You will need to provide the BORG_PASSPHRASE variable in hiera (preferably using
eyaml):

```
borgbackup::client::borgpassphrase: supersecret
```

## Usage

This module supports two types of backup jobs:

  - backing up logical volumes through backup_lv
  - backing up data from filesystem through backup_fs

### Backing up logical volumes

A systemd timer that ensures a weekly backup run can be created like this:

```
borgbackup::backup_lv { 'backstuffup':
  excludes     => ['/var/log','/tmp'],
  backupserver => 'backups.example.org',
  vg           => 'myvolume',
  lv           => 'stuff',
}
```

This assumes the logical volume to contain a paritioned disk image. In case the
logical volume directly contains a filesystem, add the rawdisk parameter:

```
borgbackup::backup_lv { 'backstuffup':
  excludes     => ['/var/log','/tmp'],
  backupserver => 'backups.example.org',
  vg           => 'myvolume',
  lv           => 'stuff',
  rawdisk      => true,
}
```

### Backing up from filesystem

To create a weekly backup of (part of) your filesystem, you can apply:

```
borgbackup::backup_fs { 'backstuffup':
  backupserver => 'backups.example.org',
  mountpoint   => '/home',
}
```

## Development

Merge requests are welcome. If you wish to share any security issues, please
contact sysadmins@tails.net. Our PGP fingerprint is:
D113CB6D5131D34BA5F0FE9E70F4F03116525F43
