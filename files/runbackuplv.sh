#!/bin/sh

set -eu

RAWDISK="0"

while getopts v:l:u:s:b:r:d option
do
  case "${option}"
  in
    v) VG=${OPTARG};;
    l) LV=${OPTARG};;
    u) BKUSER=${OPTARG};;
    s) BKSERVER=${OPTARG};;
    b) BASEDIR=${OPTARG};;
    r) REPO=${OPTARG};;
    d) RAWDISK="1";;
    *) exit 2;;
  esac
done

cleanup()
{
  cd /
  if [ ! -z "$MOUNTDIR" ]; then
    if /bin/mountpoint -q "${MOUNTDIR}"; then
      /bin/umount "${MOUNTDIR}"
    fi
    /bin/rm -rf "${MOUNTDIR}"
  fi
  if [ -b "/dev/${VG}/${SS}" ]; then
    /sbin/lvremove --force "/dev/${VG}/${SS}"
  fi
}

BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
BORG_PASSPHRASE=$(/bin/cat /etc/borgbackup/secret)
export BORG_RELOCATED_REPO_ACCESS_IS_OK
export BORG_PASSPHRASE

if [ -z "$VG" ] || [ -z "$LV" ] || [ -z "$BKUSER" ] || [ -z "$BKSERVER" ] || [ -z "$BASEDIR" ] || [ -z "$REPO" ] || [ -z "$BORG_PASSPHRASE" ]; then
  exit 2
fi

trap cleanup EXIT

MOUNTDIR=$(mktemp -d)

SS="${LV}-backup"
/sbin/lvcreate -L12G --snapshot --name "${SS}" "/dev/${VG}/${LV}"

if [ "$RAWDISK" -eq "1" ]; then
  /bin/mount -o ro "/dev/${VG}/${SS}" "${MOUNTDIR}"
else
  /usr/bin/guestmount -a "/dev/${VG}/${SS}" -i --ro "${MOUNTDIR}"
fi

BORG_OPTS=""
EXCLFILE="/etc/borgbackup/${REPO}.excludes"

if [ -f "${EXCLFILE}" ]; then
  BORG_OPTS="--exclude-from ${EXCLFILE}"
fi

cd "${MOUNTDIR}"

/usr/bin/borg create --files-cache=disabled "${BKUSER}@${BKSERVER}:${BASEDIR}/${REPO}::$(/bin/date +%g%m%d%H%M)" ./ ${BORG_OPTS}
