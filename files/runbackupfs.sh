#!/bin/sh

set -eu

while getopts m:u:s:b:r: option
do
  case "${option}"
  in
    m) MOUNTPOINT=${OPTARG};;
    u) BKUSER=${OPTARG};;
    s) BKSERVER=${OPTARG};;
    b) BASEDIR=${OPTARG};;
    r) REPO=${OPTARG};;
    *) exit 2;;
  esac
done

BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
BORG_PASSPHRASE=$(/bin/cat /etc/borgbackup/secret)
export BORG_RELOCATED_REPO_ACCESS_IS_OK
export BORG_PASSPHRASE

if [ -z "$MOUNTPOINT" ] || [ -z "$BKUSER" ] || [ -z "$BKSERVER" ] || [ -z "$BASEDIR" ] || [ -z "$REPO" ] || [ -z "$BORG_PASSPHRASE" ]; then
  exit 2
fi

BORG_OPTS=""
EXCLFILE="/etc/borgbackup/${REPO}.excludes"

if [ -f "${EXCLFILE}" ]; then
  BORG_OPTS="--exclude-from ${EXCLFILE}"
fi

cd "${MOUNTPOINT}"

/usr/bin/borg create --files-cache=disabled "${BKUSER}@${BKSERVER}:${BASEDIR}/${REPO}::$(/bin/date +%g%m%d%H%M)" ./ ${BORG_OPTS}
