# frozen_string_literal: true

require 'spec_helper'

describe 'borgbackup::backup_lv' do
  let(:hiera_config) { 'hiera-rspec.yaml' }
  let(:title) { 'namevar' }
  let(:params) do
    {
      'backupserver' => 'backups.example.org',
      'lv'           => 'myvolume',
      'vg'           => 'myvolumegroup',
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
